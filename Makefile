##############################################################################
# Start an instance of keycloak with no realms or users configured
##############################################################################
default_server:
	/usr/bin/env bash -c "\
		trap 'docker-compose down' ERR EXIT SIGHUP SIGINT SIGKILL SIGQUIT SIGTERM;\
		docker-compose run --rm --service-ports \
		-v ${PWD}/.certs:/etc/x509/https \
		keycloak -b 0.0.0.0 \
	"


##############################################################################
# Start an instance of keycloak to host acme realm
# The realm user and config files are located in `./data`
# Provides guest user with password Pa55w0rd
##############################################################################
my_server:
	/usr/bin/env bash -c "\
		trap 'docker-compose down' ERR EXIT SIGHUP SIGINT SIGKILL SIGQUIT SIGTERM;\
		docker-compose up \
	"



##############################################################################
# Stop keycloak server
##############################################################################
stop:
	docker-compose down



##############################################################################
# Export realm and users into `./data`
# Use with a running instance of the default server to alter configuration.
# Once the export has completed the container must be manually stopped using
# SIGINT.
##############################################################################
export:
	/usr/bin/env bash -c "\
	    docker run --rm --network=angular_keycloak_auth-network  --link=keycloak\
	    --name keycloak_exporter\
	    -v ${PWD}/data:/tmp/keycloak-export\
	    -e DB_DATABASE=keycloak\
	    -e DB_PASSWORD=password\
	    -e DB_USER=keycloak\
	    -e DB_VENDOR=POSTGRES\
	    -e POSTGRES_PORT_5432_TCP_ADDR=keycloakdb\
	    jboss/keycloak:latest\
	    -Dkeycloak.migration.action=export\
	    -Dkeycloak.migration.provider=dir\
	    -Dkeycloak.migration.dir=/tmp/keycloak-export\
	    -Dkeycloak.migration.usersExportStrategy=SAME_FILE\
	    -Dkeycloak.migration.realmName=acme\
	"
