from flask import Flask
from flask_cors import CORS


app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "http://angular.example.com:4200","supports_credentials": "true"}})


@app.route('/api')
def index():
    return 'Index Page'

@app.route('/api/ping', methods=['GET'])
def ping():
    return 'pong'

@app.route('/api/protected', methods=['GET'])
def protected():
    return 'protected'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
