Overview
========

Provides a docker-compose development environment that consists of the following
architectural components:

- **API Server**: Flask server accessible at *api.example.com*
- **Client**: Angular client accessible at *angular.example.com*
- **Gateway Server**: OpenResty server accessible at *gateway.example.com*
- **OpenID Connect Server**: Keycloak server accessible at *keycloak.example.com*

Please note that this is not suitable for production environment. It only serves
to demonstrate concept and workflow of the architecture.

The gateway serves as a proxy to the backend API and is implemented as an
openresty server with
[lua-resty-openidc](https://github.com/zmartzone/lua-resty-openidc) installed.

The use of a server side component to handle authentication code is being
investigated for the reasons outlined
[here](https://devforum.okta.com/t/react-sdk-signon-widget-cookies-vs-localstorage/1144).
The use of HTTP only cookie to store access token in local storage was decided
upon for reasons listed in this
[article](https://dev.to/rdegges/please-stop-using-local-storage-1i04) and [here](https://stormpath.com/blog/where-to-store-your-jwts-cookies-vs-html5-web-storage).

The gateway is used to authenticate the Angular client before
forwarding the request to the upstream API.

Developers must modify */etc/hosts* to include:
- angular.example.com
- api.example.com
- gateway.example.com
- keycloak.example.com

To use TLS connection between browser and keycloak insert tls.key and tls.crt
files in *.certs* folder located within the source root folder.
The *.certs* folder is mounted as a volume in the keycloak
docker-compose service. Alternatively, remove the volume in the docker-compose
file.

Currently, the architecture is not fully implemented to use TLS. The intention is
to provide this in future, time permitting.

Currently, when the Angular client is redirected by gateway to Keycloak login a
CORS cross origin header is displayed in console of browser. This relates to
missing Access-Control-Allow-Origin header, no host is set.

Another branch is provided, *feat/keycloak-gateway*, in the repository that uses
keycloak-gatekeeper as a gateway. The same issue occurs. Why is the error
handling?

Quickstart
----------

To start the docker-compose stack type:
```
make my_server
```

- Visit the Keycloak server at http://keycloak.example.com:8080/auth and login
as *admin* with password *password*. Review the realm and configuration settings.
- Open a new browser window and visit http://gateway.example.com/api/protected.
This triggers a proxy request to the Flask API backend via the gateway. When
prompted, login as *guest* with password *Pa55w0rd*. Upon successful
authentication 'protected' will be displayed in the browser window.
- Open a new browser window and visit http://angular.example.com:4200. A simple
Angular client application is displayed. Click on the *call api* hyperlink to
make a request to the backend Flask API via the proxy gateway. This feature is
currently not fully implemented and is experiencing a CORS cross-origin issue
when redirecting to the Keycloak login page.

Currently, a CORS cross origin error is issued when a request is sent to call
the API via the Angular client. This occurs at the point when the Keycloak
login page is redirecting on the Angular client. Not sure where the fault is
occurring yet. Is the gateway configuration failing to forward headers?


Default Keycloak Configuration
------------------------------

Login is available at http://keycloak.example.com:8080/auth with the following
credentials:
- **Login ID**: admin
- **Password**: password

The realm, *acme* has been provided that contains a single user, *guest* with
password *Pa55w0rd*. This contains a client application configured as follows:
- **Client**: client
- **Secret**:

The user profile can be edited by visiting
http://keycloak.example.com:8080/auth/realms/acme/account. A fictitious name and
email have been provided for the guest user.

Currently, the architecture is not configured to use TLS. The intention is to
provide this in future, time permitting.

A makefile has been provided with the following rules to facilitate starting the
server:
- **default_server**: start a server with no realm or guest user configured.
- **export**: save configuration to *./data* folder.
- **my_server**: start a keycloak server with acme and guest user configured.
- **stop**: stop all containers.

Default Gateway Configuration
-----------------------------
A single endpoint is provided by the API:
- */api*: Request send to http://gateway.example.com/api/* are authenticated
before sending upstream to the Flask API backend.

[lua-resty-openidc](https://github.com/zmartzone/lua-resty-openidc) has been
configured to return two HTTP only cookies containing an identity token and
access token. These are named *auth-session* and *auth-session2* respectively.
Currently, for debugging purposes these are base64 encoded with no AES
encryption.

This repository aims to provide a sample implementation for authorising
requests via the gateway, before sending upstream to the Flask API backend.

When the API is accessed via the gateway at
http://gateway.example.com/api/protected the result is displayed
in the browser upon successful authentication. Furthermore, HTTP only cookies
are received by the Angular client. The problem only occurs when
accessed via the angular client application at http://angular.example.com:4200


API Endpoints
=============
The endpoints provided by the API are as follows:
- **/api**: GET request that returns 'index' response.
- **/api/ping**: GET request that returns 'pong' response.
- **/api/protected**: GET request that returns 'protected' response.
