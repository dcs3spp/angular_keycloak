import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-call-api',
  templateUrl: './call-api.component.html',
  styles: []
})
export class CallApiComponent implements OnInit {

  response: Object;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    let headers = new HttpHeaders({'Content-Type': 'text/html', 'Accept': 'text/html'});

    this.http.get("http://gateway.example.com/api/protected", { headers: headers, responseType: 'text', withCredentials: true } )
      .subscribe(response => this.response = response);
  }

}
